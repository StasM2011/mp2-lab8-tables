#include "../include/THashTable.h"

// ��� 37 - ���������� �����

unsigned long THashTable::HashFunc(const TKey& key)
{
    unsigned int hash = 2139062143;
    for (int i = 0; i < key.length(); i++)
        hash = 37 * hash + key.at(i);
    return hash;
}
