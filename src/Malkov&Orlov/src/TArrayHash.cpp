#include "../include/TArrayHash.h"

#include <iostream>

TArrayHash::TArrayHash(int size) : THashTable() {
    TabSize = size;
    HashStep = 1;
    pRecs = new PTTabRecord[TabSize];
    for (int i = 0; i < TabSize; i++)
        pRecs[i] = 0;
    DataCount = 0;
}

TArrayHash::~TArrayHash() {
    delete [] pRecs;
}

int TArrayHash::IsFull() const {
    return DataCount >= TabSize;
}

TKey TArrayHash::GetKey(void) {
    return pRecs[CurrPos]->GetKey();
}

PTDatValue TArrayHash::GetValuePTR(void) {
    if (pRecs[CurrPos] != 0)
        return pRecs[CurrPos]->GetValuePTR();
    else
        return 0;
}

PTDatValue TArrayHash::FindRecord(TKey k) {
    CurrPos = HashFunc(k) % TabSize;
    for (int i = 0; i < TabSize; i++)
    {
        Efficiency++;
        if (pRecs[CurrPos] == 0)
            break;
        else if (pRecs[CurrPos]->GetKey() == k)
            return pRecs[CurrPos]->GetValuePTR();
        CurrPos = GetNextPos(CurrPos);
    }
    SetMessage(MSG_NO_RECORD);
    return 0;
}

void TArrayHash::InsRecord(TKey k, PTDatValue pVal) {
    CurrPos = HashFunc(k) % TabSize;
    for (int i = 0; i < TabSize; i++)
    {
        Efficiency++;
        // ���� ������ ������ ��� ���� � ��� �������, ����������
        if (pRecs[CurrPos] != 0 && pRecs[CurrPos]->GetKey() == k)
            return;
        // ���� ��� ������ - ���������
        else if (pRecs[CurrPos] == 0)
        {
            pRecs[CurrPos] = new TTabRecord(k, pVal);
            DataCount++;
            return;
        }
        // ���� �� �� ������� ������� ���� �����-�� ������, 
        // ��������� �� ��������� ������ � ������� hashstep
        CurrPos = GetNextPos(CurrPos);
    }
    SetMessage(MSG_TAB_FULL);
}

void TArrayHash::DelRecord(TKey k) {
    PTDatValue record = FindRecord(k);
    if (record != 0)
    {
        delete pRecs[CurrPos];
        pRecs[CurrPos] = 0;
        DataCount--;
    }
    else
        SetMessage(MSG_NO_RECORD);
}

int TArrayHash::Reset(void) {
    CurrPos = 0;
    GoNext();
    return 0;
}

int TArrayHash::IsTabEnded(void) const {
    return CurrPos >= TabSize;
}

int TArrayHash::GoNext(void) {
    if (IsTabEnded())
        SetMessage(MSG_TAB_FULL);
    else
        // ���� �� ������ ��������� ������ �������� ������
        while (++CurrPos < TabSize)
            if (pRecs[CurrPos] != 0)
                break;
    return GetMessage();
}
