#include "../include/TListHash.h"

TListHash::TListHash(int size) : THashTable() {
    TabSize = size;
    CurrList = 0;
    // ������� ������ ������ ��� ��������
    pList = new PTDataList[TabSize];
    for (int i = 0; i < TabSize; i++)
        pList[i] = new TDatList();
}

TListHash::~TListHash() {
    delete[] pList;
}

int TListHash::IsFull() const {
    // �� ����� ���� �����, ��������� ��� ������ ������ �����
    // ������� ��������� ��������
    return false;
}

TKey TListHash::GetKey(void) {
    PTTabRecord pRec = (PTTabRecord)(pList[CurrList]->GetDatValue());
    return pRec->GetKey();
}

PTDatValue TListHash::GetValuePTR(void) {
    PTTabRecord pRec = (PTTabRecord)(pList[CurrList]->GetDatValue());
    if (pRec != 0)
        return pRec->GetValuePTR();
    else
        return 0;
}

PTDatValue TListHash::FindRecord(TKey k)
{
    CurrList = HashFunc(k) % TabSize;
    PTDataList List = pList[CurrList];
    // �������� �� ���� ������� � ����� � ������ ��� ��������
    // ���� �� ������ ������� � ������ ������
    for (List->Reset(); !List->IsListEnded(); List->GoNext())
    {
        Efficiency++;
        if (((PTTabRecord)(List->GetDatValue()))->GetKey() == k)
            return ((PTTabRecord)(List->GetDatValue()))->GetValuePTR();
    }
    SetMessage(MSG_NO_RECORD);
    return 0;
}

void TListHash::InsRecord(TKey k, PTDatValue pVal) {
    CurrList = HashFunc(k) % TabSize;
    PTTabRecord pRec = new TTabRecord(k, pVal);
    pList[CurrList]->InsLast((PTDatValue)(pRec));
    DataCount++;
}

void TListHash::DelRecord(TKey k) {
    PTDatValue record = FindRecord(k);
    if (record != 0) {
        pList[CurrList]->DelCurrent();
        DataCount--;
    }
    else
        SetMessage(MSG_NO_RECORD);
}

int TListHash::Reset(void) {
    for (int i = 0; i < TabSize; i++)
        pList[i]->Reset();
    CurrList = 0;
    GoNext();
    return 0;
}

int TListHash::IsTabEnded(void) const {
    return CurrList >= TabSize;
}

int TListHash::GoNext(void) {
    if (IsTabEnded())
        SetMessage(MSG_TAB_FULL);
    else {
        pList[CurrList]->GoNext();
        if (pList[CurrList]->IsListEnded())
            while (!IsTabEnded() && pList[CurrList]->IsListEnded())
                CurrList++;
    }
    return GetMessage();
}
