#include "../include/TDatList.h"

PTDatLink TDatList::GetLink(PTDatValue pVal, PTDatLink pLink)
{
    return new TDatLink(pVal, pLink);
}

void TDatList::DelLink(PTDatLink pLink)
{
    if (pLink != 0)
    {
        if (pLink->pValue != 0)
            delete pLink->pValue;
        delete pLink;
    }
}

TDatList::TDatList()
{
    ListLen = 0;
    pFirst = pLast = pStop = 0;
    Reset();
}

PTDatValue TDatList::GetDatValue(TLinkPos mode) const
{
    PTDatLink temp = 0;

    switch (mode)
    {
    case FIRST: temp = pFirst; break;
    case CURRENT: temp = pCurrLink; break;
    case LAST: temp = pLast; break;
    }

    if (temp == 0)
        return 0;
    else
        return temp->GetDatValue();
}

int TDatList::SetCurrentPos(int pos)
{
    Reset();
    for (int i = 0; i < pos; i++)
        GoNext();
    return 0;
}

int TDatList::GetCurrentPos(void) const
{
    return CurrPos;
}

int TDatList::Reset(void)
{
    pPrevLink = pStop;

    if (IsEmpty())
    {
        CurrPos = -1;
        pCurrLink = pStop;
    }
    else
    {
        CurrPos = 0;
        pCurrLink = pFirst;
    };
    return 0;
}

bool TDatList::IsListEnded(void) const
{
    return (pCurrLink == pStop);
}

int TDatList::GoNext(void)
{
    if (IsListEnded() == true)
        return 0;
    else
    {
        pPrevLink = pCurrLink;
        pCurrLink = pCurrLink->GetNextDatLink();
        CurrPos++;
        return 1;
    }
}

void TDatList::InsFirst(PTDatValue pVal)
{
    PTDatLink temp = GetLink(pVal, pFirst);
    if (temp)
    {
        temp->SetNextLink(pFirst);
        pFirst = temp;
        ListLen++;
        if (ListLen == 1)
        {
            pLast = temp;
            Reset();
        }
        else if (CurrPos == 0)
            pCurrLink = temp;
        else
            CurrPos++;
    }
}

void TDatList::InsLast(PTDatValue pVal)
{
    PTDatLink temp = GetLink(pVal, pStop);

    if (temp)
    {
        if (pLast)
            pLast->SetNextLink(temp);
        pLast = temp;
        ListLen++;

        if (ListLen == 1)
        {
            pFirst = temp;
            Reset();
        }

        if (IsListEnded() == true)
            pCurrLink = temp;
    }
}

void TDatList::InsCurrent(PTDatValue pVal)
{
    if ((pCurrLink == pFirst) || IsEmpty())
        InsFirst(pVal);
    else if (IsListEnded())
        InsLast(pVal);
    else
    {
        PTDatLink temp = GetLink(pVal, pCurrLink);
        if (temp)
        {
            pPrevLink->SetNextLink(temp);
            temp->SetNextLink(pCurrLink);
            pCurrLink = temp;
            ListLen++;
        }
    }
}

void TDatList::DelFirst(void)
{
    if (!IsEmpty()) {
        PTDatLink temp = pFirst;
        pFirst = pFirst->GetNextDatLink();
        DelLink(temp);
        ListLen--;
        if (IsEmpty())
        {
            pLast = pStop;
            Reset();
        }
        else if (CurrPos == 0)
            pCurrLink = pFirst;
        else if (CurrPos == 1)
            pPrevLink = pStop;
        if (CurrPos)
            CurrPos--;
    }
}

void TDatList::DelCurrent(void)
{
    if (pCurrLink)
    {
        if ((pCurrLink == pFirst) || IsEmpty())
             DelFirst();
        else
        {
            PTDatLink temp = pCurrLink;
            pCurrLink = pCurrLink->GetNextDatLink();
            pPrevLink->SetNextLink(pCurrLink);
            DelLink(temp);
            ListLen--;
            if (pCurrLink == pLast)
            {
                pLast = pPrevLink;
                pCurrLink = pStop;
            }
        }
    }
}

void TDatList::DelList(void)
{
    while (!IsEmpty())
        DelFirst();
    CurrPos = -1;
    pFirst = pLast = pPrevLink = pCurrLink = pCurrLink = pStop;
}
