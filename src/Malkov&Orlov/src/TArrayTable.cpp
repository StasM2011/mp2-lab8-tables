#include "../include/TArrayTable.h"

TArrayTable::TArrayTable(int Size) {
    TabSize = Size;
    CurrPos = 0;
    DataCount = 0;
    pRecs = new PTTabRecord[Size];
    for (int i = 0; i < Size; i++)
        pRecs[i] = 0;
}

int TArrayTable::IsFull() const {
    return DataCount == TabSize;
}

int TArrayTable::GetTabSize() const {
    return DataCount;
}

TKey TArrayTable::GetKey(void) {
    if (DataCount == 0) {
        SetMessage(MSG_ERROR_NO_DATA);
        return "";
    }
    if (CurrPos < 0 || CurrPos >= DataCount) {
        SetMessage(MSG_ERROR_WRONG_INDEX);
        return "";
    }
    return pRecs[CurrPos]->GetKey();
}

PTDatValue TArrayTable::GetValuePTR(void) {
    if (DataCount == 0) {
        SetMessage(MSG_ERROR_NO_DATA);
        return 0;
    }
    if (CurrPos < 0 || CurrPos >= DataCount) {
        SetMessage(MSG_ERROR_WRONG_INDEX);
        return 0;
    }
    return pRecs[CurrPos]->GetValuePTR();
}

TKey TArrayTable::GetKey(TDataPos mode) {
    if (DataCount == 0) {
        SetMessage(MSG_ERROR_NO_DATA);
        return "";
    }
    switch (mode) {
    case FIRST_POS:
        return pRecs[0]->GetKey();
    case CURRENT_POS:
        return GetKey();
    case LAST_POS:
        return pRecs[DataCount - 1]->GetKey();
    }
}

PTDatValue TArrayTable::GetValuePTR(TDataPos mode) {
    if (DataCount == 0) {
        SetMessage(MSG_ERROR_NO_DATA);
        return 0;
    }
    switch (mode) {
    case FIRST_POS:
        return pRecs[0]->GetValuePTR();
    case CURRENT_POS:
        return GetValuePTR();
    case LAST_POS:
        return pRecs[DataCount - 1]->GetValuePTR();
    }
}

int TArrayTable::Reset(void) {
    CurrPos = 0;
    return CurrPos;
}

int TArrayTable::IsTabEnded(void) const {
    return CurrPos == DataCount;
}

int TArrayTable::GoNext(void) {
    if (CurrPos < DataCount) {
        CurrPos++;
        return 0;
    }
    else {
        SetMessage(MSG_ERROR_TAB_ENDED);
        return 1;
    }
}

int TArrayTable::SetCurrentPos(int pos) {
    if (pos < 0 || pos >= DataCount) {
        SetMessage(MSG_ERROR_WRONG_INDEX);
        return 0;
    }
    CurrPos = pos;
    return CurrPos;
}

int TArrayTable::GetCurrentPos(void) const {
    return CurrPos;
}