#include "../include/TBalanceTree.h"

int TBalanceTree::InsBalanceTree(PTBalanceNode & pNode, TKey k, PTDatValue pVal)
{
    int HeightIndex = HeightOK;
    if (pNode == 0) // ������� �������
    {
        pNode = new TBalanceNode(k, pVal);
        HeightIndex = HeightInc;
        DataCount++;
    }
    else if (k < pNode->GetKey())
    {
        if (InsBalanceTree((PTBalanceNode&)pNode->pLeft, k, pVal) == HeightInc)
            // ����� ������� ������ ������ ��������� ����������� - ������������
            HeightIndex = LeftTreeBalancing(pNode);
    }
    else if (k > pNode->GetKey())
    {
        if (InsBalanceTree((PTBalanceNode&)pNode->pRight, k, pVal) == HeightInc)
            // ����� ������� ������ ������� ��������� ����������� - ������������
            HeightIndex = RightTreeBalancing(pNode);
    }
    else
    {
        SetMessage(MSG_TAB_REC_DBL);
        HeightIndex = HeightOK;
    }
    return HeightIndex;
}

int TBalanceTree::LeftTreeBalancing(PTBalanceNode & pNode)
{
    int HeightIndex = HeightOK;
    switch (pNode->GetBalance()) //�������� ���������� ������������
    {
    case BalRight: // � ��������� ��� ������� ������
        pNode->SetBalance(BalOk); // ��������������� ����������
        HeightIndex = HeightOK;
        break;
    case BalOk: // � ��������� ���� ����������
        pNode->SetBalance(BalLeft); // ��������������� ������� �����
        HeightIndex = HeightInc;
        break;
    case BalLeft: // � ��������� ��� ������� ����� - ���������� ������������
        PTBalanceNode p1, p2;
        p1 = PTBalanceNode(pNode->GetLeft());
        if (p1->GetBalance() == BalLeft) // ������ 1 - ��������. LL-�������
        {
            pNode->pLeft = p1->pRight;
            p1->pRight = pNode;
            pNode->SetBalance(BalOk);
            pNode = p1;
        }
        else // ������ 2 - ���������� LR-�������
        {
            p2 = PTBalanceNode(p1->GetRight());
            p1->pRight = p2->pLeft;
            p2->pLeft = p1;
            pNode->pLeft = p2->pRight;
            p2->pRight = pNode;
            if (p2->GetBalance() == BalLeft)
                pNode->SetBalance(BalRight);
            else
                pNode->SetBalance(BalOk);
            if (p2->GetBalance() == BalRight)
                p1->SetBalance(BalLeft);
            else
                p1->SetBalance(BalOk);
            pNode = p2;
        }
        pNode->SetBalance(BalOk);
        HeightIndex = HeightOK;
    }
    return HeightIndex;
}

int TBalanceTree::RightTreeBalancing(PTBalanceNode & pNode)
{
    int HeightIndex = HeightOK;
    switch (pNode->GetBalance())
    {
    case BalLeft:
        pNode->SetBalance(BalOk);
        HeightIndex = HeightOK;
        break;
    case BalOk:
        pNode->SetBalance(BalRight);
        HeightIndex = HeightInc;
        break;
    case BalRight:
        PTBalanceNode p1, p2;
        p1 = PTBalanceNode(pNode->GetRight());
        if (p1->GetBalance() == BalRight) {
            pNode->pRight = p1->pLeft;
            p1->pLeft = pNode;
            pNode->SetBalance(BalOk);
            pNode = p1;
        }
        else
        {
            p2 = PTBalanceNode(p1->GetLeft());
            p1->pLeft = p2->pRight;
            p2->pRight = p1;
            pNode->pRight = p2->pLeft;
            p2->pLeft = pNode;
            if (p2->GetBalance() == BalRight)
                pNode->SetBalance(BalLeft);
            else
                pNode->SetBalance(BalOk);
            if (p2->GetBalance() == BalLeft)
                p1->SetBalance(BalRight);
            else
                p1->SetBalance(BalOk);
            pNode = p2;
        }
        pNode->SetBalance(BalOk);
        HeightIndex = HeightOK;
    }
    return HeightIndex;
}

void TBalanceTree::InsRecord(TKey k, PTDatValue pVal)
{
    if (IsFull())
        SetMessage(MSG_TAB_FULL);
    else
        InsBalanceTree((PTBalanceNode&)pRoot, k, pVal);
}

void TBalanceTree::DelRecord(TKey k)
{
    if (FindRecord(k) == 0)
        SetMessage(MSG_NO_RECORD);
    else
    {
        PTTreeNode tmp = pRoot;

        while (!St1.empty())
            St1.pop();
        while (tmp->GetKey() != k)
        {
            St1.push(tmp);
            if (tmp->GetKey() < k)
                tmp = tmp->GetRight();
            else
                tmp = tmp->GetLeft();
        }

        TKey k2 = tmp->GetKey();
        // �������� �����
        if ((tmp->pLeft == 0) && (tmp->pRight == 0))
        {
            if (!St1.empty())
            {
                PTTreeNode prev = St1.top();
                if (prev != 0)
                {
                    if (prev->GetRight() == tmp)
                        prev->pRight = 0;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = 0;
                }
            }
            else
                pRoot = 0;
            delete tmp;
            DataCount--;
        }
        // �������� ����� � ����� �������� (������)
        else if (tmp->pLeft == 0)
        {
            if (!St1.empty())
            {
                PTTreeNode prev = St1.top();
                if (prev != 0)
                {
                    if (prev->GetRight() == tmp)
                        prev->pRight = tmp->pRight;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = tmp->pRight;
                }
            }
            else
                pRoot = tmp->GetRight();
            delete tmp;
            DataCount--;
        }
        // �������� ����� � ����� �������� (�����)
        else if (tmp->pRight == 0)
        {
            if (!St1.empty())
            {
                PTTreeNode prev = St1.top();
                if (prev != 0)
                {
                    if (prev->GetRight() == tmp)
                        prev->pRight = tmp->pLeft;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = tmp->pLeft;
                }
            }
            else
                pRoot = tmp->GetLeft();
            delete tmp;
            DataCount--;
        }
        // �������� ����� � ����� ���������
        else
        {
            PTTreeNode down_left = tmp->GetRight();
            while (down_left->GetLeft() != 0)
                down_left = down_left->pLeft;
            down_left->pLeft = tmp->GetLeft();

            if (!St1.empty())
            {
                PTTreeNode prev = St1.top();
                if (prev != 0) {
                    if (prev->GetRight() == tmp)
                        prev->pRight = tmp->pRight;
                    if (prev->GetLeft() == tmp)
                        prev->pLeft = tmp->pRight;
                }
            }
            else
                pRoot = tmp->GetRight();
            delete tmp;
            DataCount--;
        }
        if (pRoot != 0)
        {
            if (k2 < pRoot->GetKey())
                LeftTreeBalancing((PTBalanceNode&)pRoot);
            else  if (k2 > pRoot->GetKey())
                RightTreeBalancing((PTBalanceNode&)pRoot);
        }
    }
}