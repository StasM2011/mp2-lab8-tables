#include "../include/TScanTable.h"

PTDatValue TScanTable::FindRecord(TKey k) {
    for (int i = 0; i < DataCount; i++)
        if (pRecs[i]->GetKey() == k)
            return pRecs[i];
    return 0;
}

void TScanTable::InsRecord(TKey k, PTDatValue pVal) {
    if (!IsFull()) 
        pRecs[DataCount++] = new TTabRecord(k, pVal);
}

void TScanTable::DelRecord(TKey k) {
    for (int i = 0; i < DataCount; i++) {
        if (pRecs[i]->GetKey() == k) {
            for (int j = i; pRecs[j + 1] != 0 && j < TabSize; j++)
                pRecs[j] = pRecs[j + 1];
            DataCount--;
            return;
        }
    }
}