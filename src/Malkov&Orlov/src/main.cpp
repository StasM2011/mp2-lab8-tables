#include <iostream>
#include <locale>
#include <gtest\gtest.h>
#include "../include/TStudentsProgress.h"
#include "../include/TSortTable.h"
#include "../include/TTreeTable.h"
#include "../include/TBalanceTree.h"
#include "../include/TArrayHash.h"

using namespace std;

int main(int argc, char** argv) {
    setlocale(LC_CTYPE, "Russian");

    ::testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();

    cout << "=== Sort Table ===\n";
    TStudentsProgress<TSortTable> progressSort;
    cout << progressSort.CalculateAverageMark() << endl;
    cout << progressSort.CalculateAverageMarkByGroup(1) << endl;

    cout << "=== Tree Table ===\n";
    TStudentsProgress<TTreeTable> progressTree;
    cout << progressTree.GetNumberOfExcellentStudents() << endl;
    cout << progressTree.GetIdOfGroupWithBestMarks() << endl;

    cout << "=== Balance Tree Table ===\n";
    TStudentsProgress<TBalanceTree> progressBalanceTree;
    cout << progressBalanceTree.CalculateAverageMarkByGroup(1, 2) << endl;
    cout << progressBalanceTree.CalculateAverageMarkByGroup(0) << endl;

    cout << "=== Array Hash Table ===\n";
    TStudentsProgress<TArrayHash> progressArrayHash;
    cout << progressArrayHash.GetStudentMark("�����", TStudentsData::PHYS) << endl;
    cout << progressArrayHash.GetStudentMark("���������", TStudentsData::PHYS) << endl;

    getchar();
    return 0;
}