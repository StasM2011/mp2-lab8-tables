#include "../include/TTabRecord.h"

TTabRecord::TTabRecord(TKey k, PTDatValue pval) {
    SetKey(k);
    SetValuePTR(pval);
}

void TTabRecord::SetKey(TKey k) {
    Key = TKey(k);
}

TKey TTabRecord::GetKey() const {
    return Key;
}

void TTabRecord::SetValuePTR(PTDatValue p) {
    pValue = p;
}

PTDatValue TTabRecord::GetValuePTR() const {
    return pValue;
}

TDatValue * TTabRecord::GetCopy() {
    return new TTabRecord(Key, pValue->GetCopy());
}

TTabRecord & TTabRecord::operator = (TTabRecord &tr) {
    SetKey(Key);
    SetValuePTR(tr.GetValuePTR()->GetCopy());
    return *this;
}

int TTabRecord::operator == (const TTabRecord &tr) {
    return Key.compare(tr.Key) == 0;
}

int TTabRecord::operator < (const TTabRecord &tr) {
    return Key.compare(tr.Key) < 0;
}

int TTabRecord::operator > (const TTabRecord &tr) {
    return Key.compare(tr.Key) > 0;
}