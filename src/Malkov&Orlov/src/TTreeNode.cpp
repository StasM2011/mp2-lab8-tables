#include "../include/TTreeNode.h"

PTTreeNode TTreeNode::GetLeft(void) const
{
    return pLeft;
}

PTTreeNode TTreeNode::GetRight(void) const
{
    return pRight;
}

TDatValue * TTreeNode::GetCopy()
{
    TTreeNode *temp = new TTreeNode(Key, pValue, 0, 0);
    return temp;
}