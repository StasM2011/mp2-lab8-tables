#include "../include/TArrayHash.h"

#include <gtest\gtest.h>

TEST(TArrayHash, CanGetRecord) {
    TArrayHash t;
    t.InsRecord("a", 0);
    EXPECT_EQ(t.GetDataCount(), 1);
}

TEST(TArrayHash, CanGetKey) {
    TArrayHash t;
    t.InsRecord("a", 0);
    t.Reset();
    EXPECT_EQ(t.GetKey(), "a");
}

TEST(TArrayHash, CanCheckEnded) {
    TArrayHash t;
    t.Reset();
    EXPECT_TRUE(t.IsTabEnded());
}