#include "../include/TListHash.h"

#include <gtest\gtest.h>

TEST(TListHash, CanGetRecord) {
    TListHash t;
    t.InsRecord("a", 0);
    EXPECT_EQ(t.GetDataCount(), 1);
}

TEST(TListHash, CanGetKey) {
    TListHash t;
    t.InsRecord("a", 0);
    t.Reset();
    EXPECT_EQ(t.GetKey(), "a");
}

TEST(TListHash, CanCheckEnded) {
    TListHash t;
    t.Reset();
    EXPECT_TRUE(t.IsTabEnded());
}