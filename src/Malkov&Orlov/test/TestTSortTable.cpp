#include "../include/TSortTable.h"

#include <gtest\gtest.h>

TEST(TSortTable, CanSortInsert) {
    TSortTable t;
    t.SetSortMethod(INSERT_SORT);
    t.InsRecord("b", 0);
    t.InsRecord("c", 0);
    t.InsRecord("a", 0);
    t.Reset();
    EXPECT_EQ(t.GetKey(), "a");
    t.GoNext();
    EXPECT_EQ(t.GetKey(), "b");
    t.GoNext();
    EXPECT_EQ(t.GetKey(), "c");
}

TEST(TSortTable, CanSortMerge) {
    TSortTable t;
    t.SetSortMethod(MERGE_SORT);
    t.InsRecord("b", 0);
    t.InsRecord("c", 0);
    t.InsRecord("a", 0);
    t.Reset();
    EXPECT_EQ(t.GetKey(), "a");
    t.GoNext();
    EXPECT_EQ(t.GetKey(), "b");
    t.GoNext();
    EXPECT_EQ(t.GetKey(), "c");
}

TEST(TSortTable, CanSortQuick) {
    TSortTable t;
    t.SetSortMethod(QUICK_SORT);
    t.InsRecord("b", 0);
    t.InsRecord("c", 0);
    t.InsRecord("a", 0);
    t.Reset();
    EXPECT_EQ(t.GetKey(), "a");
    t.GoNext();
    EXPECT_EQ(t.GetKey(), "b");
    t.GoNext();
    EXPECT_EQ(t.GetKey(), "c");
}