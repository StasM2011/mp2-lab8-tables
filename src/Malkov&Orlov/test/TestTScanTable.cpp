#include "../include/TScanTable.h"

#include <gtest\gtest.h>

TEST(TScanTable, CanCreate) {
    TScanTable* t;
    EXPECT_NO_THROW(t = new TScanTable());
}

TEST(TScanTable, CanGetDataCount) {
    TScanTable t;
    t.InsRecord("a", 0);
    EXPECT_EQ(t.GetDataCount(), 1);
}

TEST(TScanTable, CanGetKey) {
    TScanTable t;
    t.InsRecord("a", 0);
    EXPECT_EQ(t.GetKey(), "a");
}

TEST(TScanTable, CanCheckEnded) {
    TScanTable t;
    t.Reset();
    EXPECT_TRUE(t.IsTabEnded());
}

TEST(TScanTable, CanDelete) {
    TScanTable t;
    t.InsRecord("a", 0);
    t.DelRecord("a");
    EXPECT_TRUE(t.IsEmpty());
}

TEST(TScanTable, CanGetFirst) {
    TScanTable t;
    t.InsRecord("a", 0);
    t.InsRecord("b", 0);
    EXPECT_EQ(t.GetKey(FIRST_POS), "a");
}