#include "../include/TBalanceTree.h"

#include <gtest\gtest.h>

TEST(TBalanceTree, CanGetRecord) {
    TBalanceTree t;
    t.InsRecord("a", 0);
    EXPECT_EQ(t.GetDataCount(), 1);
}

TEST(TBalanceTree, CanGetKey) {
    TBalanceTree t;
    t.InsRecord("a", 0);
    t.Reset();
    EXPECT_EQ(t.GetKey(), "a");
}

TEST(TBalanceTree, CanCheckEnded) {
    TBalanceTree t;
    t.Reset();
    EXPECT_TRUE(t.IsTabEnded());
}