#include "../include/TTreeTable.h"

#include <gtest\gtest.h>

TEST(TTreeTable, CanGetRecord) {
    TTreeTable t;
    t.InsRecord("a", 0);
    EXPECT_EQ(t.GetDataCount(), 1);
}

TEST(TTreeTable, CanGetKey) {
    TTreeTable t;
    t.InsRecord("a", 0);
    t.Reset();
    EXPECT_EQ(t.GetKey(), "a");
}

TEST(TTreeTable, CanCheckEnded) {
    TTreeTable t;
    t.Reset();
    EXPECT_TRUE(t.IsTabEnded());
}