#pragma once

#include <string>
#include "TDatValue.h"

using namespace std;

typedef string TKey;     // ��� ����� ������

class TTabRecord;
typedef TTabRecord *PTTabRecord;

// ����� ��������-�������� ��� ������� �������
class TTabRecord : public TDatValue {

protected:    
    TKey Key;   // ���� ������
    PTDatValue pValue;   // ��������� �� ��������

public:
    TTabRecord(TKey k = "", PTDatValue pVal = NULL);    // ����������� 
    void SetKey(TKey k);                                // ���������� �������� �����
    TKey GetKey(void) const;                            // �������� �������� �����
    void SetValuePTR(PTDatValue p);                     // ���������� ��������� �� ������
    PTDatValue GetValuePTR(void) const;                       // �������� ��������� �� ������
    virtual TDatValue * GetCopy();                      // ���������� �����
    TTabRecord & operator = (TTabRecord &tr);           // ������������
    virtual int operator == (const TTabRecord &tr);     // ��������� =
    virtual int operator < (const TTabRecord &tr);      // ��������� �<�
    virtual int operator > (const TTabRecord &tr);      // ��������� �>�
    
    // ������������� ������ ��� ��������� ����� ������, ��. �����
    friend class TArrayTable;
    friend class TScanTable;
    friend class TSortTable;
    friend class TTreeNode;
    friend class TTreeTable;
    friend class TArrayHash;
    friend class TListHash;
};