#pragma once

#include "TTable.h"
#include "TTabRecord.h"

#define TabMaxSize 25
enum TDataPos { FIRST_POS, CURRENT_POS, LAST_POS };

class  TArrayTable : public TTable {
protected:
    PTTabRecord *pRecs; // ������ ��� ������� �������
    int TabSize;        // ����. ����.���������� ������� � �������
    int CurrPos;        // ����� ������� ������ (��������� � 0)
public:
    TArrayTable(int Size = TabMaxSize);     // �����������
    ~TArrayTable() {};                      // ����������
                                     
    // �������������� ������
    virtual int IsFull() const;             // ���������?
    int GetTabSize() const;                 // �-�� �������
                                 
    // ������
    virtual TKey GetKey(void);
    virtual PTDatValue GetValuePTR(void);
    virtual TKey GetKey(TDataPos mode);
    virtual PTDatValue GetValuePTR(TDataPos mode);

    // �������� ������
    virtual PTDatValue FindRecord(TKey k) = 0;              // ����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal) = 0;    // ��������
    virtual void DelRecord(TKey k) = 0;                     // ������� ������
                                               
    //���������
    virtual int Reset(void);                                // ���������� �� ������ ������
    virtual int IsTabEnded(void) const;                     // ������� ���������?
    virtual int GoNext(void);                               // ������� � ��������� ������
                                                            // (=1 ����� ���������� ��� ��������� ������ �������)
    virtual int SetCurrentPos(int pos);                     // ���������� ������� ������
    int GetCurrentPos(void) const;                          // �������� ����� ������� ������

    friend class TSortTable;
};