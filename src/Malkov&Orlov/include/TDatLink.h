#pragma once

#include "TRootLink.h"

class TDatLink;
typedef TDatLink *PTDatLink;

class TDatLink : public TRootLink {
protected:
    PTDatValue pValue;
public:
    TDatLink(PTDatValue pVal = 0, PTRootLink pN = 0) : TRootLink(pN) {
        pValue = pVal;
    }

    void SetDatValue(PTDatValue pVal) {
        pValue = pVal;
    }

    PTDatValue GetDatValue() {
        return  pValue;
    }

    PTDatLink  GetNextDatLink() {
        return (PTDatLink)pNext;
    }

    friend class TDatList;
};
