#pragma once

#include "THashTable.h"
#include "TTabRecord.h"

class  TArrayHash : public THashTable {
protected:
    PTTabRecord* pRecs;                                              // ������ ��� ������� �������
    int TabSize;                                                     // ����. ����. �-�� �������
    int HashStep;                                                    // ��� ���������� �������������
    int CurrPos;                                                     // ������ ������ ��� ���������� ������
    int GetNextPos(int pos) { return (pos + HashStep) % TabSize; };  // ����. �����.
public:
    // ���������
    static const int TAB_HASH_STEP = 1;
    TArrayHash(int size = TAB_MAX_SIZE);
    ~TArrayHash();
    // �������������� ������
    virtual int IsFull() const;                       // ���������?
    // ������
    virtual TKey GetKey(void);
    virtual PTDatValue GetValuePTR(void);
    // �������� ������
    virtual PTDatValue FindRecord(TKey k);             // ����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal);   // ��������
    virtual void DelRecord(TKey k);                    // ������� ������
    // ���������
    virtual int Reset(void);                          // ���������� �� ������ ������
    virtual int IsTabEnded(void) const;               // ������� ���������?
    virtual int GoNext(void);                         // ������� � ��������� ������

};
