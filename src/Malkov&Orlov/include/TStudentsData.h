#pragma once

#include "TDatValue.h"

struct TStudentsData : public TDatValue {
    static const int MATH = 0;
    static const int PROG = 1;
    static const int PHYS = 2;
    static const int CHEM = 3;

    static const int NUM_OF_MARKS = 4;

    int Marks[4];

    TStudentsData(int math, int prog, int phys, int chem) {
        Marks[MATH] = math;
        Marks[PROG] = prog;
        Marks[PHYS] = phys;
        Marks[CHEM] = chem;
    }

    TDatValue* GetCopy() {
        return new TStudentsData(Marks[MATH], Marks[PROG], Marks[PHYS], Marks[CHEM]);
    }
};