#pragma once

#include "TTabRecord.h"

class  TTreeNode;
typedef  TTreeNode *PTTreeNode;

class  TTreeNode : public TTabRecord {
protected:
    PTTreeNode pLeft, pRight; // ��������� �� ����������
public:
    TTreeNode(TKey k = "", PTDatValue pVal = 0, PTTreeNode pL = 0,
        PTTreeNode pR = 0) : TTabRecord(k, pVal), pLeft(pL), pRight(pR) {};
    PTTreeNode GetLeft(void) const; // ��������� �� ����� ���������
    PTTreeNode GetRight(void) const; // ��������� �� ������ ���������
    virtual TDatValue * GetCopy();  // ���������� �����
    friend class TTreeTable;
    friend class TBalanceTree;
};