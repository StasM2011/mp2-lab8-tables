#pragma once

#include <stack>
#include "TTreeNode.h"
#include "TTable.h"

class TTreeTable : public TTable {
protected:
    PTTreeNode pRoot;                   // ��������� �� ������ ������
    PTTreeNode *ppRef;                  // ����� ��������� �� �������-���������� � FindRecord
    PTTreeNode pCurrent;                // ��������� �� ������� �������
    int CurrPos;                        // ����� ������� �������
    stack<PTTreeNode> St;               // ���� ��� ���������
    stack<PTTreeNode> St1;              // ���� ��� ��������
    void DeleteTreeTab(PTTreeNode pNode); // ��������
public:
    TTreeTable() : TTable() { CurrPos = 0; pRoot = pCurrent = 0; ppRef = 0; }
    ~TTreeTable() { DeleteTreeTab(pRoot); } // ����������
                                            // �������������� ������
    virtual int IsFull() const;            //���������?
                                            //�������� ������
    virtual PTDatValue FindRecord(TKey k);  // ����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal); // ��������
    virtual void DelRecord(TKey k);         // ������� ������
                                            
    // ���������
    virtual TKey GetKey(void);
    virtual PTDatValue GetValuePTR(void);
    virtual int Reset(void);                // ���������� �� ������ ������
    virtual int IsTabEnded(void) const;     // ������� ���������?
    virtual int GoNext(void);               // ������� � ��������� ������
                                            //(=1 ����� ���������� ��� ��������� ������ �������)
};