#pragma once

#include "THashTable.h"
#include "TDatList.h"
#include "TTabRecord.h"

class  TListHash : public THashTable {
protected:
    PTDataList* pList;                                              // ������ ��� ������� ���������� �� ������ ������� 
    int TabSize;                                                    // ������ ������� ����������
    int CurrList;                                                   // ������, � ������� ���������� �����
public:
    TListHash(int size = TAB_MAX_SIZE);                              // �����������
    ~TListHash();
    // �������������� ������
    virtual int IsFull() const;                                       // ���������?
    // ������
    virtual TKey GetKey(void);
    virtual PTDatValue GetValuePTR(void);
    // �������� ������
    virtual PTDatValue FindRecord(TKey k);                            // ����� ������
    virtual void InsRecord(TKey k, PTDatValue pVal);                  // ��������
    virtual void DelRecord(TKey k);                                   // ������� ������
    // ���������
    virtual int Reset(void);                          // ���������� �� ������ ������
    virtual int IsTabEnded(void) const;               // ������� ���������?
    virtual int GoNext(void);                         // ������� � ��������� ������
};