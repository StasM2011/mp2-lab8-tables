#pragma once

#define MSG_OK 0
#define MSG_ERROR_TAB_ENDED 1
#define MSG_ERROR_NO_DATA 2
#define MSG_ERROR_WRONG_INDEX 3
#define MSG_TAB_REC_DBL 4
#define MSG_TAB_FULL 5
#define MSG_NO_RECORD 6
#define MSG_OUT_OF_RANGE 7

class TDataCom {
protected:

    int _Message;

public:

    TDataCom() : _Message(MSG_OK) {}

    void SetMessage(int message) {
        _Message = message;
    }

    int GetMessage() {
        int m = _Message;
        _Message = MSG_OK;
        return m;
    }

};