#pragma once

#include <fstream>
#include <vector>
#include "TStudentsData.h"
#include "TTable.h"

using namespace std;

template <class TDataContainer>
class TStudentsProgress {
private:
    vector<TTable*> Tables;
    int GroupsNum;

    int GetNumberOfGroups() {
        ifstream file;
        int result = 0;
        for (int i = 1; ; i++) {
            file.open(GetFileName(i));
            if (file.is_open()) {
                result++;
                file.close();
            }
            else
                return result;
        }
    }

    void LoadStudentsProgress() {
        ifstream file;
        for (int i = 1; i <= GroupsNum; i++) {
            TTable* container = new TDataContainer();
            file.open(GetFileName(i));
            while (!file.eof()) {
                string name;
                int math, prog, phys, chem;
                file >> name;
                file >> math >> prog >> phys >> chem;
                container->InsRecord(name, new TStudentsData(math, prog, phys, chem));
            }
            container->Reset();
            Tables.push_back(container);
            file.close();
        }
    }

    string GetFileName(int groupId) {
        string fileName = "students/group";
        fileName += '0' + groupId;
        fileName += ".txt";
        return fileName;
    }

public:

    TStudentsProgress() {
        GroupsNum = GetNumberOfGroups();
        LoadStudentsProgress();
    }

    // ������ ��������
    TStudentsData* GetStudentData(TKey studentName) {
        for (auto it = Tables.begin(); it != Tables.end(); it++) {
            PTDatValue result = (*it)->FindRecord(studentName);
            if (result != 0)
                return (TStudentsData*) result;
        }
        return 0;
    }

    // ������ ��������
    int GetStudentMark(TKey studentName, int discipline) {
        return GetStudentData(studentName)->Marks[discipline];
    }

    // ������� ������ ��������
    double CalculateStudentAverageMark(TKey studentName) {
        TStudentsData* data = GetStudentData(studentName);
        double result = 0;
        for (int i = 0; i < TStudentsData::NUM_OF_MARKS; i++)
            result += data->Marks[i];
        return result / TStudentsData::NUM_OF_MARKS;
    }

    // ������� ������ ����� �������� ���� �����
    double CalculateAverageMark(int discipline = -1) {
        double result = 0;
        double count = 0;
        for (int i = 0; i < GroupsNum; i++) {
            result += CalculateAverageMarkByGroup(i, discipline);
            count++;
        }
        return result / count;
    }

    // ������� ������ ����� �������� ���������� ������
    // ���� ������� (discipline) �� ����� -1, �� ������� �� ����������� ��������
    double CalculateAverageMarkByGroup(int group, int discipline = -1) {
        double result = 0;
        double count = 0;
        TTable* table = Tables[group];
        table->Reset();
        while (!table->IsTabEnded()) {
            TStudentsData* data = (TStudentsData*)(table->GetValuePTR());
            if (data)
                for (int i = 0; i < TStudentsData::NUM_OF_MARKS; i++) {
                    if (discipline == -1 || i == discipline) {
                        result += data->Marks[i];
                        count++;
                    }
                }
            table->GoNext();
        }
        return result / count;
    }

    // ��������� id ������ � ������ ������������� �� �������� ��� ���� ���������
    int GetIdOfGroupWithBestMarks(int discipline = -1) {
        double max = 0;
        int bestId = -1;
        for (int i = 0; i < GroupsNum; i++) {
            double mark = CalculateAverageMarkByGroup(i, discipline);
            if (mark > max) {
                max = mark;
                bestId = i;
            }
        }
        // ++ ��������� ������ ���������� ������� � 1
        return ++bestId;
    }

    // ������ ���������� � 1, ������� ���� group = 0 - �� ������� ���������� ���������� �� ���� �������
    // ��������� ���������� ��������� ����������
    int GetNumberOfExcellentStudents(int group = 0) {
        int result = 0;
        for (int i = 0; i < GroupsNum; i++) {
            if (group == 0 || group == (i + 1)) {
                TTable* table = Tables[i];
                table->Reset();
                while (!table->IsTabEnded()) {
                    int markSum = 0;
                    TStudentsData* data = (TStudentsData*)table->GetValuePTR();
                    if (data != 0)
                        for (int j = 0; j < TStudentsData::NUM_OF_MARKS; j++)
                            markSum += data->Marks[j];
                    // ���� ��� �������
                    if (markSum == TStudentsData::NUM_OF_MARKS * 5)
                        result++;
                    table->GoNext();
                }
            }
        }
        return result;
    }

};